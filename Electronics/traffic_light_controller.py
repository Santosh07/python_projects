import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(12, GPIO.OUT)    #FW - Green Light
GPIO.setup(16, GPIO.OUT)    #FW - Yellow Light
GPIO.setup(22, GPIO.OUT)    #FW - Red Light
GPIO.setup(26, GPIO.OUT)    #PR - Green Light
GPIO.setup(28, GPIO.OUT)    #PR - Yellow Light
GPIO.setup(30, GPIO.OUT)    #PR - Red Light
GPIO.setup(2, GPIO.IN)

flag1 = 0
flag2 = 0
while True:
    x = GPIO.input(2)
    if x == 1 and flag1 == 0:  #button not pressed - default state
        GPIO.output(12, GPIO.HIGH)
        GPIO.output(16, GPIO.LOW)
        GPIO.output(22, GPIO.LOW)
        GPIO.output(26, GPIO.LOW)
        GPIO.output(28, GPIO.LOW)
        GPIO.output(30, GPIO.HIGH)
        flag1 = 1
    elif x == 0 and flag2 == 0:    #state 1
        GPIO.output(12, GPIO.LOW)
        GPIO.output(16, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(16, GPIO.LOW)
        GPIO.output(22, GPIO.HIGH)
        GPIO.output(30, GPIO.LOW)
        GPIO.output(26, GPIO.HIGH)
        GPIO.output(28, GPIO.LOW)
        flag2 = 1
    elif x == 1 and flag1 == 1:     #state 2
        GPIO.setup(26, GPIO.LOW)
        GPIO.setup(28, GPIO.HIGH)
        time.sleep(1)
        GPIO.setup(28, GPIO.LOW)
        GPIO.setup(30, GPIO.HIGH)
        GPIO.setup(22, GPIO.LOW)
        GPIO.setup(16, GPIO.LOW)
        GPIO.setup(12, GPIO.HIGH)
        flag1 = 0
        flag2 = 0
        
