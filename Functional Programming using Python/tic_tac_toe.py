#Tic-Tac-Toe game

store = {1:"_ ", 2:"_ ", 3:"_ ", 4:"_ ", 5:"_ ", 6:"_ ", 7:"_ ", 8:"_ ", 9:"_ "}
flag = 0
for i in store:
    if i == 3 or i == 6:
        print(i, ":", store[i])
    else:
        print(i, ":", store[i], end="")

for i in range(1, 10, 1):
    if i%2 == 1:
        print("\nPlayer_X turn")
        player_x = int(input())
        while store[player_x] != "_ ":
            player_x = int(input())
        store[player_x] = "x"
        for i in range(1, 10, 1):
            if i == 3 or i == 6:
                print(i, ":", store[i])
            else:
                print(i, ":", store[i], end="")
        if store[1]==store[2]==store[3]=="x" or store[4]==store[5]==store[6]=="x" or store[7]==store[8]==store[9]=="x" or store[1]==store[5]==store[9]=="x" or store[3]==store[5]==store[7]=="x" or store[1]==store[4]==store[7]=="x" or store[2]==store[5]==store[8]=="x" or store[3]==store[6]==store[9]=="x":
                print("\nPlayer X wins")
				flag = 1
                break
    else:
        print("\nPlayer_O turn")
        player_o = int(input())
        while store[player_o] != "_ ":
            player_o = int(input())
        store[player_o] = "o"
        for i in range(1, 10, 1):
            if i == 3 or i == 6:
                print(i, ":", store[i])
            else:
                print(i, ":", store[i], end="")
        if store[1]==store[2]==store[3]=="o" or store[4]==store[5]==store[6]=="o" or store[7]==store[8]==store[9]=="o" or store[1]==store[5]==store[9]=="o" or store[3]==store[5]==store[7]=="o" or store[1]==store[4]==store[7]=="o" or store[2]==store[5]==store[8]=="o" or store[3]==store[6]==store[9]=="o":
                print("\nPlayer O wins")
				flag = 1
                break

if flag == 0:
	print("Its a Tie")
