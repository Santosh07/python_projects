#Exercise 3.14: Extending program 13 to allow only three attempts. Additionally, printing the list of passwords tried on a successful login

count = 3
flag = 0
saved_pwd = " "
pwd = input ("Please enter your password. You have %i attempts left: " % count)
pwd_list = []

while (pwd != saved_pwd):
    count = count - 1
    if (count > 0):
        pwd_list.append(pwd)
        pwd = input("Access Denied! You have %i attempts left. Please try again: " % count)
        flag = 1
    elif (count == 0):
        print ("Sorry! You have used up all your attempts. Please come again later")
   

if (flag == 1):
    print ("Access Granted! List of invalid entries before getting the right one:", pwd_list)
else:
    print ("You got it right on the first attempt. Access Granted!")

"""
Results begin

Please enter your password. You have 3 attempts left: shetty
Access Denied! You have 2 attempts left. Please try again: Oliv
Access Denied! You have 1 attempts left. Please try again: Oliver
Access Granted! List of invalid entries before getting the right one: ['shetty', 'Oliv']

Please enter your password. You have 3 attempts left: she
Access Denied! You have 2 attempts left. Please try again: she
Access Denied! You have 1 attempts left. Please try again: shet
Sorry! You have used up all your attempts. Please come again later

Please enter your password. You have 3 attempts left: Oliver
You got it right on the first attempt. Access Granted!

Results end
"""
