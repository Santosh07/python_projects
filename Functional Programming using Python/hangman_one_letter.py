#With shallow copy, change to the original changes the duplicate as well
#whereas with deep copy, a change to original does not change the duplicate
import random
sports = ["Tennis", "Football", "Basketball", "Soccer", "Volleyball"]
secret = random.choice(sports)
secretword = list(secret)
#secretword1 = secretword            #shallow copy
secretword1 = secretword.copy()     #deep copy
print(secretword1)
blanks = ["_"]*len(secretword)
print(blanks)
while True:
    if blanks == secretword1:
        break
    print("Guess the word one letter at a time")
    guess = input()
    if guess in secretword:
        inde = secretword.index(guess)
        secretword[inde] = "-"
        blanks[inde] = guess
        print(blanks)
#        print(secretword1)

print("You got it")
