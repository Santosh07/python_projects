import pygame
import random
from pygame.locals import *

pygame.init()

screen = pygame.display.set_mode((640, 480))

color = (255, 0, 0)
black = (0, 0, 0)

x = 320
y = 240

x_rect_l = 0
y_rect_l = 0
speedx = 1
speedy = 1
x_rect_r = 615
y_rect_r = 0
y_speed_rm_up = 0
y_speed_rm_down = 0
y_speed_lm_up = 0
y_speed_lm_down = 0
dx = 2
dy = 2

y_rect_lm = 235
y_rect_rm = 190

green = (0, 255, 0)
blue = (150, 150, 255)
unknown = (150, 150, 150)
white = (255, 255, 255)
y_speed_lm = 5
y_speed_rm = 5
score_r = 0
score_l = 0
FPS = 50

clock = pygame.time.Clock()

display = True

def disp_score(score, x, y):
    font = pygame.font.SysFont(None, 25)
    screen_text = font.render(("Score: "+str(score)), True, unknown)
    screen.blit(screen_text, [x, y])
                              
while display:
    screen.fill(blue)
    pygame.draw.circle(screen, color, (x, y), 10)
    pygame.draw.rect(screen, green, (x_rect_l, y_rect_lm, 25, 50))
    pygame.draw.rect(screen, green, (x_rect_r, y_rect_rm, 25, 50))
    pygame.draw.line(screen, white, (320,0), (320, 480), 10)
    disp_score(score_r, 550, 50)
    disp_score(score_l, 90, 50)
    
    pygame.display.update()

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
#-----------code to move paddle with continuous press---------------
##        if event.type == KEYDOWN:
##            if event.key == K_UP and y_rect_rm >= 0:
##                y_speed_rm = -5
##                y_rect_rm = y_rect_rm + y_speed_rm
##            elif event.key == K_DOWN and y_rect_rm <= 430:
##                y_speed_rm = 5
##                y_rect_rm = y_rect_rm + y_speed_rm
##            elif event.key == K_w and y_rect_lm >= 0:
##                y_speed_lm = -5
##                y_rect_lm = y_rect_lm + y_speed_lm
##            elif event.key == K_s and y_rect_lm <= 430:
##                y_speed_lm = 5
##                y_rect_lm = y_rect_lm + y_speed_lm
#---------------------------------------------------------

#-----------code to move paddle with just one press-----------------------------
        if event.type == KEYDOWN:
            if event.key == K_UP :  #and y_rect_rm >= 0
                y_speed_rm_up = 1
            if event.key == K_DOWN :    #and y_rect_rm <= 430
                y_speed_rm_down = 1
            if event.key == K_w :   #and y_rect_lm >= 0
                y_speed_lm_up = 1
            if event.key == K_s :   #and y_rect_lm >= 0
                y_speed_lm_down = 1
        if event.type == KEYUP:
            if event.key == K_UP :  #and y_rect_rm >= 0
                y_speed_rm_up = 0
            if event.key == K_DOWN :    #and y_rect_rm <= 430
                y_speed_rm_down = 0
            if event.key == K_w :       #and y_rect_lm >= 0
                y_speed_lm_up = 0
            if event.key == K_s :       #and y_rect_lm >= 0
                y_speed_lm_down = 0
    if y_speed_rm_up == 1 and y_rect_rm>=0:
            y_rect_rm = y_rect_rm - y_speed_rm
    if y_speed_rm_down == 1 and y_rect_rm<=430:
            y_rect_rm = y_rect_rm + y_speed_rm
    if y_speed_lm_up == 1 and y_rect_lm>=0:
            y_rect_lm = y_rect_lm - y_speed_rm
    if y_speed_lm_down == 1 and y_rect_lm<=430:
            y_rect_lm = y_rect_lm + y_speed_rm
#------------code for ball movement and collision detection------------
    if x>=630:
        speedx = random.randint(-5, 0)
        score_l = score_l + 1
        disp_score(score_l, 90, 50)
        #speedy = 1
    elif x<=10:
        speedx = random.randint(0, 5)
        score_r = score_r + 1
        disp_score(score_r, 550, 50)
        #speedy = 1
    elif y>=470:
        speedy=random.randint(-5, 0)
        #speedx = 1
    elif y<=10:
        speedy=random.randint(0, 5)
        #speedx = 1
    elif x-10<=x_rect_l+25 and y_rect_lm+50>=y>=y_rect_lm:
        speedx = random.randint(0, 5)
    elif x+10>=x_rect_r and y_rect_rm+50>=y>=y_rect_rm:
        speedx = random.randint(-5, 0)
    
    
    x = x + speedx
    y = y + speedy
#-------------------------------------------------------------------------
    clock.tick(FPS)
    

pygame.quit()


