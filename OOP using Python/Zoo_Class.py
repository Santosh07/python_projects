class Zoo:
    name = ""
    zooname = ""
    zookeeper = ""
    zootiming = ""
    zoo_max = ""
    animals = {}
    def __init__(self, a, b, c, d):
        #print("ZOO::init")
        self.name = a
        self.zooname = b
        self.zookeeper = c
        self.zootiming = d
        #self.zoo_max = e
##        print("Zoo_Owner:"+self.name, end="   ")
##        print("Zoo_Name:"+self.zooname, end="   ")
##        print("Zoo_Keeper:"+self.zookeeper, end="   ")
##        print("Zoo_Timing:"+str(self.zootiming), end="   ")
##        print("Zoo_Max_Animals:"+str(self.zoo_max))
        
    def add_animal(self, a):
        for j,m in a.items():
            if j in self.animals:
                self.animals[j] = self.animals[j] + m
            #self.animals.append(j)
            else:
                self.animals[j] = m

    def remove_animal(self, a):
        for j,m in a.items():
            if j in self.animals:
                self.animals[j] -= m
            else:
                print("The animal does not exist to be removed")
            #self.animals.pop(j)
        #print(self.zooname,":", "Animals in the Zoo:", self.animals)

    def animals_print(self):
        print("Zoo_Owner:"+self.name, end="   ")
        print("Zoo_Name:"+self.zooname, end="   ")
        print("Zoo_Keeper:"+self.zookeeper, end="   ")
        print("Zoo_Timing:"+str(self.zootiming), end="   ")
        z = self.animals.values()
        y = sum(z)
        print("Zoo_Max_Animals:"+str(y))
        print(self.zooname,":", "Animals in the Zoo:")
        for k in self.animals:
            print(k, ":", self.animals[k])
        
            
        
                
