from Zoo_revision import Zoo

zoo1 = Zoo("Santosh", "California", "Andy", 9)
#zoo1.zooname = "California Zoo"
#print(zoo1.zooname)

zoo2 = Zoo("Sandeep", "Oregon", "Tom", 10)
#zoo2.zooname = "Oregon Zoo"
#print(zoo2.zooname)

zoo3 = Zoo("Sandhya", "Texas", "Ryan", 11)
#zoo3.zooname = "Texas Zoo"
#print(zoo3.zooname)
zoo1.add_animal({"Lion":2, "Tiger":5, "Deer":8})
##zoo1.add_animal(["Elephant"])
##zoo1.add_animal(["Lion", "Tiger", "Deer"])
#zoo1.remove_animal(["Tiger", "Lion"])
zoo1.remove_animal({"Tiger":1, "Lion":1})
zoo1.animals_print()

