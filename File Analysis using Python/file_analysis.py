'''Program to count the number of words in the file'''

f_handle = open("wordlist.100000", "r")

##count = 0
##for words in f_handle:
##    count += 1
##print("The total number of words in the file are", count)


'''Program to finnd the letter with which maximum words begin'''

letter_dic = {}

for words in f_handle:
    first_letter = words[0]
    if first_letter in letter_dic:
        letter_dic[first_letter] += 1
    else:
        letter_dic[first_letter] = 1

max_value = max(letter_dic.values())

for key, value in letter_dic.items():
    if value == max_value:
        print("The letter with which maximum words begin is", key, "and the number of words that begin with that letter are", value)
        break

'''Program to print all the words that end with the letter "y" '''

##for words in f_handle:
##    words = words.strip()
##    first_letter = words[-1]
##    if first_letter == "y":
##        print(words)

'''Program to print all the palindromes from the file'''

##for words in f_handle:
##    words = words.strip()
##    words_rev = words[::-1]
##    if words_rev == words:
##        print(words)

'''Program to print all the semordnilap from the file
Semordnilap are basically words whose reverse is also present in the file'''

##words_dic = {}
##
##for words in f_handle:
##    words = words.strip()
##    words_dic[words] = 1
##
##for words in words_dic:
##    words_rev = words[::-1]
##    if words_rev in words_dic:
##        print(words)

'''Program to print all the words from the file that contain the letters "a", "b", "c" '''

##for words in f_handle:
##    if "a" in words and "b" in words and "c" in words:
##        print(words)
